import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class camera_page_review_card extends StatelessWidget {

  final String imagePath;
  final String username;

  const camera_page_review_card({Key? key, required this.imagePath, required this.username})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(30),
      child: Column(
        children: [
          Flexible(
            child: ListView(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,20,0,0),
                      child: Text("ADL User",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25.0,
                            color: Color(0xffA6243C),
                            //backgroundColor: Color(0xffA6243C),
                          )),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20,0,0,0),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        maxLines: null,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            hintText: 'Write your feedback'
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,

                  // TODO: avoid fixed numbers & make it more responsive
                  height: 500,
                  padding: const EdgeInsets.fromLTRB(10, 150, 10, 10),
                  child: Image.file(File(imagePath),
                    errorBuilder:
                        (BuildContext context, Object exception, StackTrace? stackTrace) {

                      return const Text('Please Select an Image from the gallery');
                    },
                    //width: MediaQuery.of(context).size.width *1,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}