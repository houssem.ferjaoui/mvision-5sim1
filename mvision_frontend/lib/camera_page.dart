// ignore_for_file: prefer_const_constructors

import 'dart:convert';

import 'package:adl_frontend/TakePictureScreen.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'camera_page_review_card.dart';
import 'dart:io' as Io;
import 'package:http/http.dart' as http;




class camera_page extends StatefulWidget {
  final CameraDescription camera;
  const camera_page({
    Key? key,
    required this.camera,
  }) : super(key: key);


  @override
  State<camera_page> createState() => _camera_pageState();
}


class _camera_pageState extends State<camera_page> {
  Color background_color = const Color(0xffFFF6F8);
  late XFile _image = new XFile("");
  late String img64;


  final ImagePicker _picker = ImagePicker();

  Future<void> print_response (String img64) async {
    try {
      var url = Uri.parse('http://192.168.1.50:3000/script');
      var response = await http.post(url, body: {'image': img64});
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
    } on Exception catch(e){
      print("problems bro");
    }
  }

  _imgFromGallery() async {
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image!;
      final imagepath = _image.path;
      final bytes = Io.File(imagepath).readAsBytesSync();

      img64= base64Encode(bytes);
      print_response(img64);
      //print(await http.read(Uri.parse('https://example.com/foobar.txt')));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // scaffold stuff
      backgroundColor: background_color,

      // Appbar
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: background_color,
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Color(0xffA6243C)),
            onPressed: () =>
                print('return button clicked') //Navigator.of(context).pop(),
        ),
        title: Text("Return",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 23.0,
              color: Color(0xffA6243C),
              //backgroundColor: Color(0xffA6243C),
            )),
      ),

      // body stuff
      body: Center(
        child: Container(
          // TODO: Replace username with the actual logged in username
          child: camera_page_review_card(imagePath: _image.path, username: 'Samir Pessiron'),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xffA6243C),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.cancel_outlined,
              color: Colors.white,
            ),
            title: Text("Annuler",
                style: TextStyle(
                  //fontWeight: FontWeight.bold,
                  //fontSize: 30.0,
                  color: Colors.white,
                  //backgroundColor: Color(0xffA6243C),
                )),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.verified_user_outlined,
              color: Colors.white,
            ),
            title: Text("Postuler",
                style: TextStyle(
                  //fontWeight: FontWeight.bold,
                  //fontSize: 30.0,
                  color: Colors.white,
                  //backgroundColor: Color(0xffA6243C),
                )),
          ),
        ],
        //currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],

        //onTap: _onItemTapped,
      ),
      floatingActionButton: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // TAKE PICTURE WITH CAMERA BUTTON
            FloatingActionButton(
              onPressed: () async {

                // Add your onPressed code here!
                print("button clicked");
                final result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TakePictureScreen(camera: widget.camera)),);

            setState(() {

              _image = result;
              final imagepath = _image.path;
              final bytes = Io.File(imagepath).readAsBytesSync();

              img64= base64Encode(bytes);
              print_response(img64);

              /*final imagepath = _image.path;
                final bytes = Io.File(imagepath).readAsBytesSync();
                img64= base64Encode(bytes);
                print_response(img64);*/
            });
            },
              child: Icon(Icons.camera_alt_outlined),
              backgroundColor: Color(0xffA6243C),
              tooltip: 'Increment Counter',
              elevation: 10,
            ),

            // SELECT PICTURE FROM GALLERY BUTTON
            FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                print("button clicked");
                _imgFromGallery();
              },
              child: Icon(Icons.photo_album_outlined),
              backgroundColor: Color(0xffA6243C),
              tooltip: 'Increment Counter',
              elevation: 10,
            ),
          ],
        ),
      ),
      //floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}

